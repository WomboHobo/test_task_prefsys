import json
import threading
import re
import urllib
from time import time

import boto3


link_regex = re.compile(
    r'^(?:http|ftp)s?://' # http:// or https://
    r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
    r'localhost|' #localhost...
    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
    r'(?::\d+)?' # optional port
    r'(?:/?|[/?]\S+)$',
    re.IGNORECASE
)


s3 = boto3.resource('s3')
result = {}
lock = threading.Lock()


def get_average_latency(link, retries):
    time_sum = 0

    for i in range(retries):
        stream = urllib.request.urlopen(link)
        start_time = time()
        output = stream.read()
        end_time = time()
        stream.close()
        time_sum += end_time - start_time

    with lock:
        result.update(
            {
                link: "{} seconds, {} retries".format(time_sum/retries, retries)
            }
        )


def get_elapsed_time(links, retries):
    threads = []
    for link in links:
        if re.match(link_regex, link) is not None:
            link_thread = threading.Thread(target=get_average_latency, args=(link, retries))
            threads.append(link_thread)
            link_thread.start()
        else:
            with lock:
                result.update(
                    {
                        link: 'Is not a link!'
                    }
                )

    for thread in threads:
        thread.join()


def lambda_handler(event, context):
    try:
        links = list(event.get('website_links', []))
        retries = int(event.get('retries', 3))
    except TypeError:
        return {
            'statusCode': 400,
            'body': 'Improper parameters.',
        }
    get_elapsed_time(links, retries)
    return {
        'statusCode': 200,
        'body': json.dumps(result),
    }
